using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeColor : MonoBehaviour
{
    public bool goGreen;
    float time;
    

    // Start is called before the first frame update
    void Start()
    {
        time = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > 2)
        {   
            GetComponent<Renderer>().material.SetColor("_Color", goGreen ? Color.green : Color.blue);
        }
    }
}
